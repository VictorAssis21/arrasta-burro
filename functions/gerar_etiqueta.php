<?php


    // header('Content-type: application/pdf');


require('../dompdf/lib/html5lib/Parser.php');
require('../dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
require_once '../dompdf/lib/php-svg-lib/src/autoload.php';
require_once '../dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

$html = "<html>
<head>
<link href='../lib/css/jquery-ui.css' rel='stylesheet'>

  <style>

    @page {
      size: 100mm 30.5mm;
      margin: 0;
      margin-header: 0
      margin-footer: 0;
    }
    @page noheader {
      odd-header-name: _blank;
      even-header-name: _blank;
      odd-footer-name: _blank;
      even-footer-name: _blank;
    }
    #layou{
      border: 0.264583333mm solid black;
      width:100mm;
      height:30.5mm;
      margin: 0;
      position:relative;
    }
    .draggable{
      width: 2.166666667mm;
      height: 1.583333333mm;
      background: blue;
      position: absolute;
    }
    h6{
      padding:0;
      margin:0;
    }

  </style>
  <script src='../lib/js/jquery-ui.js'></script>
</head>
<body>
  <div id='layou' class='ui-droppable row'>
    <h6 class='draggable ui-draggable ui-draggable-handle ui-draggable-dragging' style='margin-left: 41.275mm; margin-top: 9.525mm;'>
      CÓDIGO: 22772
    </h6>
  </div>
</body>
</html>";


$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper(array(0, 0, 100, 30.5), 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

 ?>
