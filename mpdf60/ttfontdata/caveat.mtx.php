<?php
$name='CaveatBrush-Regular';
$type='TTF';
$desc=array (
  'CapHeight' => 660.0,
  'XHeight' => 420.0,
  'FontBBox' => '[-133 -300 1044 900]',
  'Flags' => 4,
  'Ascent' => 960.0,
  'Descent' => -300.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 835.0,
);
$unitsPerEm=1000;
$up=-135;
$ut=54;
$strp=252;
$strs=54;
$ttffile='/var/www/html/eportal/vendors/mataburro/mpdf60/ttfonts/CaveatBrush-Regular.ttf';
$TTCfontID='0';
$originalsize=295568;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='caveat';
$panose=' 0 0 0 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 960, -300, 0
// usWinAscent/usWinDescent = 960, -300
// hhea Ascent/Descent/LineGap = 960, -300, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>