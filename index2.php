<!DOCTYPE HTML>
<html lang="pt-br" ng-app="promotores">
  <head>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>

    </style>
    <link href="lib/css/estilo.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="lib/js/interact.js"></script>



    <title>Editor de Etiquetas</title>
  <head>
  <body>
    <div class="container">

      <div class="row">
        <div class="col-md-12 col-sm-12">
           <div id="layout" style="width:100mm; height:35mm;">
             <!-- <img id="layout" src="images.jpeg" class="dropzone" style="position:relative;visibility:hidden;z-index:2;" onclick="alert('clicou');"> -->

              <table id="grid" style="width:100%; height:100%">
                <!-- <tbody> -->
                  <!-- <tr style="width:100%; height: 30%;">
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                  </tr>
                  <tr style="width:100%; height: 30%;">
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                  </tr>
                  <tr style="width:100%; height: 30%;">
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                    <td style="width:25%;" ondragover="arrastou(event);" ondrop="soltou(event)"></td>
                  </tr> -->
                <!-- </tbody> -->
              </table>
           </div>
        <div class="col-md-12 col-sm-12">

          <!-- <div id="items" ondragover="arrastou(event);" ondrop="soltou(event)"> -->
            <div id="items">
            <table>
              <!-- <tr>
                <td><img style="width: 80px; height: 40px;" src="lib/img/barra.png" class="draggable" id="barra" draggable="true" ondragstart="selecionou(event);"></td>
                <td><h6 class="draggable drag-drop" id="descricao" draggable="true" ondragstart="selecionou(event);">RAÇÃO PARA CÃES ADULTO FORM PREMIUM</h6></td>
                <td><h6 class="draggable drag-drop" id="codigo" draggable="true" ondragstart="selecionou(event);">CÓDIGO: 22772</h6></td>
                <td><h6 class="draggable drag-drop" id="preco" draggable="true" ondragstart="selecionou(event);">R$ 139,90</h6></td>
                <td><h6 class="draggable drag-drop" id="data" draggable="true" ondragstart="selecionou(event);">13/06/2018</h6></td>
              </tr> -->
              <tr>
                <td><img style="width: 80px; height: 40px;" src="lib/img/barra.png" draggable="true" ondragstart="selecionou(event);" id="barra"></td>
                <td><h6 draggable="true" ondragstart="selecionou(event);" id="descricao" >R</h6></td>
                <td><h6 draggable="true" ondragstart="selecionou(event);" id="codigo" >CÓDIGO: 22772</h6></td>
                <td><h6 draggable="true" ondragstart="selecionou(event);" id="preco" >R$ 139,90</h6></td>
                <td><h6 draggable="true" ondragstart="selecionou(event);" id="data">28/04/2018</h6></td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
    <button type="button" onclick="enviar();">Gerar PDF</button>

    <div class='row'>
      <div class='input-group'>
        <input id="largura" type="number" class='form-group' placeholder="Largura (em mm)">
      </div>
      <div class='input-group'>
        <input id="altura" type="number" class='form-group' placeholder="Altura (em mm)">
      </div>
    </div>
    <button type="button" onclick="alterar();">Gerar PDF</button>



    <script>
      function alterar(){

          var altura = $("#altura").val();
          var largura = $("#largura").val();
          console.log(altura);
          console.log(largura);

          $("#layout").css({"width":largura+"mm", "height": altura+"mm"});

          cellsAltura = parseInt(altura / 10);
          cellsLargura = largura / 25;
          var table = "";
          // var tableRow = "";
          var tableCell = "";

          for(var i= 0; i!=cellsAltura ; i++){
            console.log(tableRow);
            var tableRow = "<tr style=' table-layout: fixed; '>";
            for (var j=0; cellsLargura!=j; j++){
              tableCell += "<td ondragover='arrastou(event);' ondrop='soltou(event)' ></td>";
            }
            console.log(tableCell);
            tableRow+=(tableCell);
            tableRow+="</tr>";
            tableCell="";
            table +=tableRow;
          }
          console.log(table);
          $("#grid").append(table);

      }

      function enviar(){

        window.open('functions/gerar_etiqueta3.php?x='+15+'&y='+10+'');

      }


      function selecionou(ev){
        ev.dataTransfer.setData("text", ev.target.id);

      }
      function arrastou(ev){
        ev.preventDefault();

      }
      function soltou(ev){
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
      }
    </script>



  </body>
