<!DOCTYPE HTML>
<html lang="pt-br" ng-app="promotores">
  <head>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="lib/css/estilo.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="lib/css/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js" crossorigin="anonymous"></script>
    <script src="lib/js/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js" integrity="sha384-THVO/sM0mFD9h7dfSndI6TS0PgAGavwKvB5hAxRRvc0o9cPLohB0wb/PTA7LdUHs" crossorigin="anonymous"></script>
    <!-- <script src="lib/js/interact.js"></script> -->
    <title>Editor de Etiquetas</title>
    <style>
    /*#draggable { width: 30px; height: 30px; }*/
    #barra{
      width: 80px; height: 40px; padding: 0.5em; margin-bottom: 20px; float: left;
    }
    .draggable{
      width: 80px; height: 40px; padding: 0.5em;  margin-bottom: 20px; float: left;
    }
    #layou{
      clear: both; border: 1px solid black; width:100mm;  height:30.5mm;
    }
  </style>
  <head>
  <body>
    <div class="container">


           <div id="layou" style="position:relative;">

           </div>

          <div id="items">
            <table>
              <tr>
                <td><img src="lib/img/barra.png" class="draggable barra" id="barra"></td>
                <td><h6 class="draggable" id="descricao" >R</h6></td>
                <td><h6 class="draggable" id="codigo" >CÓDIGO: 22772</h6></td>
                <td><h6 class="draggable" id="preco" >R$ 139,90</h6></td>
                <td><h6 class="draggable" id="data">28/04/2018</h6></td>
              </tr>
            </table>
          </div>

          <button type="button" onclick="enviar();">Gerar PDF</button>
          <button type="button" onclick="transformar();">Gerar Imagem</button>
      </div>

    <script>
    // $( function() {
      $( ".draggable" ).draggable({
        helper:"clone",
        grid: [10, 10],
        revert: "invalid"
        // containment:"document"
      });
      $( "#layou" ).droppable({
        over:function(event,ui){
          // console.log(ui.position);
        },
        accept: '.draggable',
        drop:function(event, ui) {
            // ui.draggable.detach().appendTo($(this));
            var droppedTable = $(ui.helper).clone();
            droppedTable.appendTo($(this));
        }
      });
    // } );
      // var canvas = document.getElementById('layout');
      // var context = canvas.getContext('2d');
      //
      // canvas.addEventListener('mousemove', function(evt) {
      //   var mousePos = getMousePos(canvas, evt);
      //   var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
      //   console.log(messge);
      // }, false);

    function enviar(){
      window.open('functions/gerar_etiqueta2.php');
    }
    function transformar(){
      html2canvas(document.querySelector("#layou")).then(canvas => {
        document.body.appendChild(canvas);
        console.log(canvas);
      });
    }


      function dragMoveListener (event) {
        var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
        target.style.transform =
          'translate(' + x + 'px, ' + y + 'px)';

        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
      }

      // this is used later in the resizing and gesture demos
      window.dragMoveListener = dragMoveListener;
    </script>
  </body>
